USE [master]
GO
/****** Object:  Database [QLBS]    Script Date: 5/6/2023 6:36:24 PM ******/
CREATE DATABASE [QLBS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLBS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\QLBS.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QLBS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\QLBS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [QLBS] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLBS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLBS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLBS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLBS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLBS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLBS] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLBS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLBS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLBS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLBS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLBS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLBS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLBS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLBS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLBS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLBS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLBS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLBS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLBS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLBS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLBS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLBS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLBS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLBS] SET RECOVERY FULL 
GO
ALTER DATABASE [QLBS] SET  MULTI_USER 
GO
ALTER DATABASE [QLBS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLBS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLBS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLBS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QLBS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QLBS] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'QLBS', N'ON'
GO
ALTER DATABASE [QLBS] SET QUERY_STORE = ON
GO
ALTER DATABASE [QLBS] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [QLBS]
GO
/****** Object:  Table [dbo].[NhaXuatBan]    Script Date: 5/6/2023 6:36:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhaXuatBan](
	[MaNXB] [nvarchar](15) NOT NULL,
	[TenNXB] [nvarchar](30) NOT NULL,
	[DiaChiNXB] [nvarchar](50) NOT NULL,
	[DienThoai] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_NhaXuatBan] PRIMARY KEY CLUSTERED 
(
	[MaNXB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sach]    Script Date: 5/6/2023 6:36:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sach](
	[MaSach] [nvarchar](15) NOT NULL,
	[TenSach] [nvarchar](30) NOT NULL,
	[MaTL] [nvarchar](15) NOT NULL,
	[MaTG] [nvarchar](15) NOT NULL,
	[MaNXB] [nvarchar](15) NOT NULL,
	[SoLuongTon] [int] NOT NULL,
	[GiaNhap] [int] NOT NULL,
	[GiaBan] [int] NOT NULL,
 CONSTRAINT [PK_Sach] PRIMARY KEY CLUSTERED 
(
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TacGia]    Script Date: 5/6/2023 6:36:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TacGia](
	[MaTG] [nvarchar](15) NOT NULL,
	[TenTG] [nvarchar](30) NOT NULL,
	[LienLac] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_TacGia] PRIMARY KEY CLUSTERED 
(
	[MaTG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TheLoai]    Script Date: 5/6/2023 6:36:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TheLoai](
	[MaTL] [nvarchar](15) NOT NULL,
	[TenTL] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_TheLoai] PRIMARY KEY CLUSTERED 
(
	[MaTL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[NhaXuatBan] ([MaNXB], [TenNXB], [DiaChiNXB], [DienThoai]) VALUES (N'NXB1', N'Kim Đồng', N'22 An Trạch', N'0912038271')
INSERT [dbo].[NhaXuatBan] ([MaNXB], [TenNXB], [DiaChiNXB], [DienThoai]) VALUES (N'NXB2', N'Tiền Phong', N'30 Nguyễn Du', N'0382299671')
INSERT [dbo].[NhaXuatBan] ([MaNXB], [TenNXB], [DiaChiNXB], [DienThoai]) VALUES (N'NXB3', N'Tâm hồn', N'12 Nguyễn Phong Sắc', N'0988615782')
GO
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S001', N'Số đỏ', N'TT', N'TG003', N'NXB2', 4, 3000, 15000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S002', N'Làm đĩ', N'HC', N'TG003', N'NXB2', 3, 3000, 12000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S003', N'Giông tố', N'TT', N'TG003', N'NXB2', 0, 5000, 15000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S004', N'Đồng hào có ma', N'CB', N'TG001', N'NXB3', 5, 5000, 22000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S005', N'Cô giáo Minh ', N'HC', N'TG001', N'NXB3', 7, 3000, 24000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S006', N'Tấm lòng vàng', N'HC', N'TG001', N'NXB3', 8, 3000, 26000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S007', N'Những năm sáu mười', N'TT', N'TG009', N'NXB1', 8, 2000, 29000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S008', N'Chữ người tử tù', N'HC', N'TG002', N'NXB1', 10, 10000, 40000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S009', N'Lão hạc', N'CB', N'TG005', N'NXB3', 5, 4000, 10000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S010', N'Chí Phèo', N'CB', N'TG005', N'NXB3', 2, 7000, 22000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S011', N'Tác Phẩm và lời bình', N'TT', N'TG005', N'NXB3', 5, 9000, 26000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S012', N'Làng', N'HC', N'TG004', N'NXB1', 4, 5000, 45000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S013', N'Quê hương', N'HC', N'TG006', N'NXB1', 3, 6000, 45000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S014', N'Từ thế giới bên kia', N'KD', N'TG010', N'NXB1', 4, 4000, 16000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S015', N'Người khách lạ', N'KD', N'TG010', N'NXB1', 4, 8000, 16000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S016', N'Bóng ma bên cửa', N'KD', N'TG010', N'NXB1', 8, 10000, 25000)
INSERT [dbo].[Sach] ([MaSach], [TenSach], [MaTL], [MaTG], [MaNXB], [SoLuongTon], [GiaNhap], [GiaBan]) VALUES (N'S017', N'Truyện Kiều', N'HC', N'TG008', N'NXB3', 4, 6000, 80000)
GO
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG001', N'Nguyễn Công Hoan', N'0988617734')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG002', N'Nguyễn Tuân', N'0912038972')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG003', N'Vũ Trọng Phụng', N'0921267345')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG004', N'Kim Lân', N'0988615782')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG005', N'Nam Cao', N'091203825')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG006', N'Xuân Quỳnh', N'0921267542')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG008', N'Nguyễn Du', N'0988671568')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG009', N'Huy C?n', N'0988615792')
INSERT [dbo].[TacGia] ([MaTG], [TenTG], [LienLac]) VALUES (N'TG010', N'Nguyễn Ngọc Ngạn', N'0988675287')
GO
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'CB', N'Châm biếm')
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'GG', N'GIật gân')
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'HC', N'Hư cấu')
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'HH', N'Hài hước')
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'KD', N'Kinh dị')
INSERT [dbo].[TheLoai] ([MaTL], [TenTL]) VALUES (N'TT', N'Tiểu thuyết')
GO
ALTER TABLE [dbo].[Sach]  WITH CHECK ADD  CONSTRAINT [FK_Sach_NhaXuatBan] FOREIGN KEY([MaNXB])
REFERENCES [dbo].[NhaXuatBan] ([MaNXB])
GO
ALTER TABLE [dbo].[Sach] CHECK CONSTRAINT [FK_Sach_NhaXuatBan]
GO
ALTER TABLE [dbo].[Sach]  WITH CHECK ADD  CONSTRAINT [FK_Sach_TacGia] FOREIGN KEY([MaTG])
REFERENCES [dbo].[TacGia] ([MaTG])
GO
ALTER TABLE [dbo].[Sach] CHECK CONSTRAINT [FK_Sach_TacGia]
GO
ALTER TABLE [dbo].[Sach]  WITH CHECK ADD  CONSTRAINT [FK_Sach_TheLoai] FOREIGN KEY([MaTL])
REFERENCES [dbo].[TheLoai] ([MaTL])
GO
ALTER TABLE [dbo].[Sach] CHECK CONSTRAINT [FK_Sach_TheLoai]
GO
USE [master]
GO
ALTER DATABASE [QLBS] SET  READ_WRITE 
GO
