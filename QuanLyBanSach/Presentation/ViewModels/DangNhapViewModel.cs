﻿using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Presentation.ViewModels.Base;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class DangNhapViewModel: BaseViewModel
    {
        private TaiKhoanRepository taiKhoanRepository;
        public DangNhapViewModel()
        {
            taiKhoanRepository = new TaiKhoanRepositoryImpl();
        }

        public bool KiemTraDangNhap(string tenTaiKhoan, string matKhau)
        {
            var taiKhoanList = taiKhoanRepository.LayDanhSachTaiKhoan();
            for (int i = 0; i < taiKhoanList.Count; i++)
            {
                if (tenTaiKhoan == taiKhoanList[i].TenTaiKhoan && matKhau == taiKhoanList[i].MatKhau)
                {
                    Const.LoaiTaiKhoan = taiKhoanList[i].LoaiTaiKhoan;
                    return true;
                }
            }
            return false;

        }
    }
}
