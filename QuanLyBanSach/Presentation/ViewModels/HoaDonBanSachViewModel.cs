﻿using System.Data;
using QuanLyBanSach.Data.Connections;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class HoaDonBanSachViewModel
    {
        public DataTable LayTaCa(string MATGBS, string MATLBS, string MANXBBS, string TenSachBS)
        {
            string CommandText = "SELECT a.* FROM SACH a LEFT JOIN TheLoai b ON a.MaTL = b.MaTL";
            CommandText += " LEFT JOIN TacGia c ON a.MaTG = c.MaTG";
            CommandText += " LEFT JOIN NhaXuatBan d ON a.MaNXB = d.MaNXB";
            CommandText += " WHERE 1=1";
            if (TenSachBS.Trim() != "")
            {
                CommandText += " AND TenSach LIKE N'%" + TenSachBS.Trim() + "%'";
            }
            if (MATGBS != "")
            {
                CommandText += " AND TenTG LIKE N'%" + MATGBS + "%'";
            }
            if (MATLBS != "")
            {
                CommandText += " AND TenTL LIKE N'%" + MATLBS + "%'";
            }
            if (MANXBBS != "")
            {
                CommandText += " AND TenNXB LIKE N'%" + MANXBBS + "%'";
            }

            return DatabaseConnection.DataTable_Sql(CommandText);
        }
    }
}
