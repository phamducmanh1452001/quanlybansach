﻿using System.Data;
using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Presentation.ViewModels.Base;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class TimKiemNhaXuatBanViewModel: BaseViewModel
    {
        private NhaXuatBanRepository nhaXuatBanRepository;

        public TimKiemNhaXuatBanViewModel()
        {
            nhaXuatBanRepository = new NhaXuatBanRepositoryImpl();
        }

        public DataTable LayTatCa()
        {
            var nxbList = nhaXuatBanRepository.LayTatCa();
            return ConvertListToDataTable<NhaXuatBan>(nxbList);
        }
    }
}
