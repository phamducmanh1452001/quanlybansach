﻿using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class CapNhatTacGiaViewModel
    {
        private TacGiaRepository tacGiaRepository;
        public CapNhatTacGiaViewModel()
        {
            tacGiaRepository = new TacGiaRepositoryImpl();
        }

        public bool XoaTacGiaTheoMa(string maSach)
        {
            return tacGiaRepository.XoaTacGia(maSach);
        }

        public bool CapNhatTacGiaTheoMa(string maTG, TacGia tacGia)
        {
            return tacGiaRepository.CapNhatTacGia(maTG, tacGia);
        }

        public bool NhapTacGia(TacGia tacGia)
        {
            return tacGiaRepository.NhapTacGia(tacGia);
        }
    }
}
