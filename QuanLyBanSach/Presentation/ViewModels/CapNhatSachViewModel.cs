﻿using System.Text.RegularExpressions;
using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class CapNhatSachViewModel
    {
        private SachRepository sanPhamRepository;
        public CapNhatSachViewModel()
        {
            sanPhamRepository = new SachRepositoryImpl();
        }

        public bool XoaSachTheoMa(string maSach)
        {
            return sanPhamRepository.XoaSachTheoMa(maSach);
        }

        public bool CapNhatSachTheoMa(string maSach, Sach sach)
        {
            return sanPhamRepository.CapNhatSachTheoMa(maSach, sach);
        }

        public bool NhapSach(Sach sach)
        {
            return sanPhamRepository.NhapSach(sach);
        }
    }
}
