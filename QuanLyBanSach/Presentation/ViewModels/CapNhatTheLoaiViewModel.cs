﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class CapNhatTheLoaiViewModel
    {
        private TheLoaiRepository theLoaiRepository;

        public CapNhatTheLoaiViewModel()
        {
            theLoaiRepository = new TheLoaiRepositoryImpl();
        }

        public bool XoaTheLoaiTheoMa(string maTL)
        {
            return theLoaiRepository.XoaTheLoaiTheoMa(maTL);
        }

        public bool CapNhatTheLoaiTheoMa(string maTL, TheLoai nxb)
        {
            return theLoaiRepository.CapNhatTheLoaiTheoMa(maTL, nxb);
        }

        public bool NhapTheLoai(TheLoai tl)
        {
            return theLoaiRepository.NhapTheLoai(tl);
        }
    }
}
