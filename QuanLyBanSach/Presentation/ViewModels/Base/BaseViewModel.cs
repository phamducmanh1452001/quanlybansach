﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Presentation.ViewModels.Base
{
    public class BaseViewModel
    {
        protected DataTable ConvertListToDataTable<T>(List<T> objectList)
        {
            DataTable dataTable = new DataTable();

            // Create columns based on object properties
            foreach (var field in typeof(T).GetFields())
            {
                dataTable.Columns.Add(field.Name, field.FieldType);
            }

            // Add rows with object values
            foreach (T obj in objectList)
            {
                var fields = obj.GetType().GetFields();
                DataRow dataRow = dataTable.NewRow();

                foreach (var field in fields)
                { 
                    var fieldValue = field.GetValue(obj);
                    dataRow[field.Name] = fieldValue;
                }
                dataTable.Rows.Add(dataRow);


            }

            return dataTable;
        }
    }
}
