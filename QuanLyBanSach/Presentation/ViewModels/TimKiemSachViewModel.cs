﻿using System.Data;
using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Presentation.ViewModels.Base;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class TimKiemSachViewModel: BaseViewModel
    {
        private SachRepository sachRepository;

        public TimKiemSachViewModel()
        {
            sachRepository = new SachRepositoryImpl();
        }

        public DataTable TimTheoNhaXuatBan(string maNXB)
        {
            var sachList = sachRepository.TimKiemSachTheoNhaXuatBan(maNXB);
            return ConvertListToDataTable<Sach>(sachList);
        }

        public DataTable TimTheoTacGia(string maTG)
        {
            var sachList = sachRepository.TimKiemSachTheoTacGia(maTG);
            return ConvertListToDataTable<Sach>(sachList);
        }

        public DataTable TimTheoTheLoai(string maTL)
        {
            var sachList = sachRepository.TimKiemSachTheoTheLoai(maTL);
            return ConvertListToDataTable<Sach>(sachList);
        }

        public DataTable LayTatCaSach()
        {
            var sachList = sachRepository.LayTatCa();
            return ConvertListToDataTable<Sach>(sachList);
        }
    }
}
