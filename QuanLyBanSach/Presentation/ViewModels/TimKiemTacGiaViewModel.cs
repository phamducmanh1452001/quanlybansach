﻿
using System.Data;

using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Presentation.ViewModels.Base;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class TimKiemTacGiaViewModel: BaseViewModel
    {
        private TacGiaRepository tacGiaRepository;

        public TimKiemTacGiaViewModel() {
            tacGiaRepository = new TacGiaRepositoryImpl();
        }

        public DataTable LayTatCa()
        {
            var tacGiaList = tacGiaRepository.LayTatCa();
            return ConvertListToDataTable<TacGia>(tacGiaList);
        }

       
    }
}
