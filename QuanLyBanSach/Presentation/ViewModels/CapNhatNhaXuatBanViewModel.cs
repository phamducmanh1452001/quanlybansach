﻿using QuanLyBanSach.Data.Repositories;
using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class CapNhatNhaXuatBanViewModel
    {
        private NhaXuatBanRepository nxbRepository;
        public CapNhatNhaXuatBanViewModel()
        {
            nxbRepository = new NhaXuatBanRepositoryImpl();
        }

        public bool XoaNhaXuatBanTheoMa(string maSach)
        {
            return nxbRepository.XoaNhaXuatBanTheoMa(maSach);
        }

        public bool CapNhatNhaXuatBanTheoMa(string maSach, NhaXuatBan nxb)
        {
            return nxbRepository.CapNhatNhaXuatBanTheoMa(maSach, nxb);
        }

        public bool NhapNhaXuatBan(NhaXuatBan nxb)
        {
            return nxbRepository.NhapNhaXuatBan(nxb);
        }
    }
}
