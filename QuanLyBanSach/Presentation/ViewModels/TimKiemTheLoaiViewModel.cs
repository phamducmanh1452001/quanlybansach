﻿using System.Data;
using QuanLyBanSach.Data.Repositories;

using QuanLyBanSach.Domain.Repositories;
using QuanLyBanSach.Presentation.ViewModels.Base;

namespace QuanLyBanSach.Presentation.ViewModels
{
    public class TimKiemTheLoaiViewModel: BaseViewModel
    {
        private TheLoaiRepository theLoaiRepository;

        public TimKiemTheLoaiViewModel() {
            theLoaiRepository = new TheLoaiRepositoryImpl();
        }

        public DataTable LayTatCa()
        {
            var theLoaiList = theLoaiRepository.LayTatCa();
            return ConvertListToDataTable(theLoaiList);
        }

        
    }
}
