﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanSach.Presentation.ViewModels;

namespace QuanLyBanSach
{
    public partial class DangNhap : Form
    {
        private DangNhapViewModel dangNhapViewModel;

        public DangNhap()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw, true);

            dangNhapViewModel = new DangNhapViewModel();
        }
        


        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(Control ctrl in this.Controls)
            {
                if(ctrl is MdiClient)
                {
                    ctrl.BackColor = Color.White;
                }
            }
              
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dangNhapViewModel.KiemTraDangNhap(txt_User.Text,txt_Pass.Text))
            {
                Main frm = new Main();
                MessageBox.Show("Đăng nhập thành công!");
                // this.Hide();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Tên người dùng hoặc mật khẩu sai vui lòng kiểm tra lại! ", "Thông báo", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                txt_Pass.Clear();
                txt_User.Clear();
            
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = MessageBox.Show("Bạn có chắc chắn muốn thoát không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(DialogResult==DialogResult.Yes)
            {
                this.Close();
            }

        }

        private void txt_User_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                txt_Pass.UseSystemPasswordChar = false;
            }
            else
            {
                txt_Pass.UseSystemPasswordChar = true;
            }
        }

        private void lbl_tieude_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
