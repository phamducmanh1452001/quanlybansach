﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;

namespace QuanLyBanSach
{
    public partial class TimKiemTheLoai : Form
    {
        private TimKiemTheLoaiViewModel timKiemTheLoaiViewModel;
        private TimKiemSachViewModel timKiemSachViewModel;

        public TimKiemTheLoai()
        {
            InitializeComponent();
            timKiemTheLoaiViewModel = new TimKiemTheLoaiViewModel();
            timKiemSachViewModel = new TimKiemSachViewModel();
        }

        private void TimKiemTheLoai_Load(object sender, EventArgs e)
        {
            DataTable dt = timKiemTheLoaiViewModel.LayTatCa();
            cboChonTheLoai.DataSource = dt;
            cboChonTheLoai.DisplayMember = "TenTL";
            cboChonTheLoai.ValueMember = "TenTL";
            if (cboChonTheLoai.Items.Count > 1)
            {
                cboChonTheLoai.SelectedIndex = -1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand command = new SqlCommand();
            string maTL = cboChonTheLoai.SelectedValue.ToString();
            var dt = timKiemSachViewModel.TimTheoTheLoai(maTL);
            dgvkqTheLoai.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
