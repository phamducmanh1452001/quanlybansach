﻿using System;
using System.Data;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

using QuanLyBanSach.Presentation.ViewModels;
using QuanLyBanSach.Data.Connections;

namespace QuanLyBanSach
{
    public partial class HoaDonBanSach : Form
    {
        private HoaDonBanSachViewModel hoaDonBanSachViewModel;
        private TimKiemTacGiaViewModel timKiemTacGiaViewModel;
        private TimKiemTheLoaiViewModel timKiemTheLoaiViewModel;
        private TimKiemNhaXuatBanViewModel timKiemNhaXuatBanViewModel;
        private TimKiemSachViewModel timKiemSachViewModel;

        private DataTable dt;
        private DataRow dr;

        public HoaDonBanSach()
        {
            InitializeComponent();
            hoaDonBanSachViewModel = new HoaDonBanSachViewModel();
            timKiemTacGiaViewModel = new TimKiemTacGiaViewModel();
            timKiemTheLoaiViewModel = new TimKiemTheLoaiViewModel();
            timKiemSachViewModel = new TimKiemSachViewModel();
            timKiemNhaXuatBanViewModel = new TimKiemNhaXuatBanViewModel();

            dt = new DataTable();
            dr = dt.NewRow();
        }

        private void btnTimHDBS_Click(object sender, EventArgs e)
        {
            TaiDanhSach();
        }
        private void TaiDanhSach()
        {
            string MATGBS = cboTGBS.Text.Trim();
            string MATLBS = cboTLBS.Text.Trim();
            string MANXBBS = cboNXBBS.Text.Trim();
            string TenSachBS = txtTenSachBS.Text.Trim();

            var dt = hoaDonBanSachViewModel.LayTaCa(MATGBS, MATLBS, MANXBBS, TenSachBS);
            dgvKetQua.DataSource = dt;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = MessageBox.Show("Bạn chắc chắn muốn thoát chứ ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(DialogResult==DialogResult.Yes)
            {
                this.Close();
            }
            else
            {

            }
            
        }

        private void txtTongTien_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnThemVaoGio_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.dgvKetQua.Rows[dgvKetQua.CurrentCell.RowIndex];
            string MaSach = row.Cells[0].Value.ToString();
            string TenSach = row.Cells[0].Value.ToString();
            decimal GiaNhap = decimal.Parse(row.Cells[6].Value.ToString());

            dr = dt.NewRow();
            dr["MaSach"] = MaSach;
            dr["TenSach"] = TenSach;
            dr["SoLuong"] = txtSLBan.Value;
            dt.Rows.Add(dr);
            dgvGioHang.DataSource = dt;

            txtTongTien.Value = txtTongTien.Value + (txtSLBan.Value * GiaNhap);
        }

        public void HoaDonBanSach_Load(object sender, EventArgs e)
        {
            
            dt.Columns.Add("MaSach");
            dt.Columns.Add("TenSach");
            dt.Columns.Add("SoLuong");

            loadUtilsData();
            // load combo box TacGia

        }

        private void loadUtilsData()
        {
            var tacGiaDt = timKiemTacGiaViewModel.LayTatCa();
            cboTGBS.DisplayMember = "TenTacGia";
            cboTGBS.ValueMember = "TenTacGia";
            cboTGBS.DataSource = tacGiaDt;

            if (cboTGBS.Items.Count > 1)
            {
                cboTGBS.SelectedIndex = -1;
            }


            var theLoaiDt = timKiemTheLoaiViewModel.LayTatCa();
            cboTLBS.DisplayMember = "TenTL";
            cboTLBS.ValueMember = "MaTL";
            cboTLBS.DataSource = theLoaiDt;

            if (cboTLBS.Items.Count > 1)
            {
                cboTLBS.SelectedIndex = -1;
            }

            //Load combo box NXB
            var nxbDt = timKiemNhaXuatBanViewModel.LayTatCa();
            cboNXBBS.DisplayMember = "TenNXB";
            cboNXBBS.ValueMember = "MaNXB";
            cboNXBBS.DataSource = nxbDt;
            if (cboNXBBS.Items.Count > 1)
            {
                cboNXBBS.SelectedIndex = -1;
            }


            // Load datagrid view KetQua
            var sachDt = timKiemSachViewModel.LayTatCaSach();
            dgvKetQua.DataSource = sachDt;
        }

        private void txtTenSachBS_TextChanged(object sender, EventArgs e)
        {
            
        }

        public void btnMoi_Click(object sender, EventArgs e)
        {

            loadUtilsData();
        }

        private void dgvKetQua_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = dgvKetQua.CurrentRow.Index;
            txtTenSachBS.Text = dgvKetQua.Rows[i].Cells[1].Value.ToString();
            cboNXBBS.SelectedValue = dgvKetQua.Rows[i].Cells[4].Value.ToString();
            cboTLBS.SelectedValue= dgvKetQua.Rows[i].Cells[2].Value.ToString();
            cboTGBS.SelectedValue= dgvKetQua.Rows[i].Cells[3].Value.ToString();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Bạn có chắc chắn muốn tính tiền không ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {                
                for (int index = 0; index < dgvGioHang.Rows.Count; index++)
                {

                    var slTon = dgvGioHang.Rows[index].Cells["SoLuong"].Value;
                    var maSach = dgvGioHang.Rows[index].Cells["MaSach"].Value;
                    string CommandText = "UPDATE Sach SET SoLuongTon = SoLuongTon - " + (slTon == null ? "0" : slTon.ToString()) + " WHERE MaSach ='"+ maSach + "'";
                    DatabaseConnection.Execute_NonSQL(CommandText);
                }
                MessageBox.Show("Tính thành công ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                ExportToExcel(ConvertDataGridViewToDataTable(dgvGioHang));
                TaiDanhSach();
            };
            
        }

        private void btnXoaKhoiGio_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
                
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        public DataTable ConvertDataGridViewToDataTable(DataGridView dataGridView)
        {
            DataTable dataTable = new DataTable();

            // Create columns in the DataTable
            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                dataTable.Columns.Add(column.Name, column.ValueType);
            }

            // Populate rows in the DataTable
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                DataRow dataRow = dataTable.NewRow();

                // Populate cell values in the DataRow
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dataRow[cell.ColumnIndex] = cell.Value;
                }

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        private void ExportToExcel(DataTable dataTable)
        {
            // Khởi tạo một đối tượng Excel mới
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook workbook = excelApp.Workbooks.Add();
            Excel.Worksheet worksheet = workbook.ActiveSheet;

            // Đặt tên các cột trong Excel
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                worksheet.Cells[1, i + 1] = dataTable.Columns[i].ColumnName;
            }

            // Ghi dữ liệu vào Excel
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dataTable.Rows[i][j].ToString();
                }
            }

            // var timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString();
            // Lưu file Excel
            workbook.SaveAs("HoaDon" + GetUniqueString() + ".xlsx");


            // Đóng Workbook và Application Excel
            workbook.Close();
            excelApp.Quit();
        }

        private string GetUniqueString()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            return g.ToString();
        }
    }

   
}
