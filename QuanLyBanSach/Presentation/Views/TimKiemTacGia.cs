﻿using System;

using System.Data;

using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;

namespace QuanLyBanSach
{
    public partial class TimKiemTacGia : Form
    {
        private TimKiemTacGiaViewModel timKiemTacGiaViewModel;
        private TimKiemSachViewModel timKiemSachViewModel;

        public TimKiemTacGia()
        {
            InitializeComponent();
            timKiemTacGiaViewModel = new TimKiemTacGiaViewModel();
            timKiemSachViewModel = new TimKiemSachViewModel();
        }


        private void TimKiemTacGia_Load(object sender, EventArgs e)
        {
            DataTable dt = timKiemTacGiaViewModel.LayTatCa();
            cboChonTacGia.DisplayMember = "TenTacGia";
            cboChonTacGia.ValueMember = "TenTacGia";

            cboChonTacGia.DataSource = dt;

            if (cboChonTacGia.Items.Count > 1)
            {
                cboChonTacGia.SelectedIndex = -1;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string maTG = cboChonTacGia.SelectedValue.ToString();
            var dt = timKiemSachViewModel.TimTheoTacGia(maTG);
            dgvkqTacGia.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
