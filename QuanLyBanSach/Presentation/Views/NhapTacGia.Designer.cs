﻿namespace QuanLyBanSach
{
    partial class NhapTacGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NhapTacGia));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaTacGia = new System.Windows.Forms.TextBox();
            this.txtLienLac = new System.Windows.Forms.TextBox();
            this.btnKhoiTaoTacGia = new System.Windows.Forms.Button();
            this.btnSuaTacGia = new System.Windows.Forms.Button();
            this.btnXoaTacGia = new System.Windows.Forms.Button();
            this.btnThemTacGia = new System.Windows.Forms.Button();
            this.dgvNhapTacGia = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtTenTacGia = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapTacGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cập nhật tác giả";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã tác giả";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên tác giả";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Liên lạc";
            // 
            // txtMaTacGia
            // 
            this.txtMaTacGia.Location = new System.Drawing.Point(123, 93);
            this.txtMaTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaTacGia.Name = "txtMaTacGia";
            this.txtMaTacGia.Size = new System.Drawing.Size(167, 22);
            this.txtMaTacGia.TabIndex = 4;
            this.txtMaTacGia.TextChanged += new System.EventHandler(this.txtMaTacGia_TextChanged);
            // 
            // txtLienLac
            // 
            this.txtLienLac.Location = new System.Drawing.Point(123, 171);
            this.txtLienLac.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLienLac.Name = "txtLienLac";
            this.txtLienLac.Size = new System.Drawing.Size(167, 22);
            this.txtLienLac.TabIndex = 6;
            // 
            // btnKhoiTaoTacGia
            // 
            this.btnKhoiTaoTacGia.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoiTaoTacGia.Image")));
            this.btnKhoiTaoTacGia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKhoiTaoTacGia.Location = new System.Drawing.Point(500, 135);
            this.btnKhoiTaoTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnKhoiTaoTacGia.Name = "btnKhoiTaoTacGia";
            this.btnKhoiTaoTacGia.Size = new System.Drawing.Size(141, 36);
            this.btnKhoiTaoTacGia.TabIndex = 22;
            this.btnKhoiTaoTacGia.Text = "Clear";
            this.btnKhoiTaoTacGia.UseVisualStyleBackColor = true;
            this.btnKhoiTaoTacGia.Click += new System.EventHandler(this.btnKhoiTaoTacGia_Click);
            // 
            // btnSuaTacGia
            // 
            this.btnSuaTacGia.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaTacGia.Image")));
            this.btnSuaTacGia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSuaTacGia.Location = new System.Drawing.Point(500, 93);
            this.btnSuaTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSuaTacGia.Name = "btnSuaTacGia";
            this.btnSuaTacGia.Size = new System.Drawing.Size(141, 38);
            this.btnSuaTacGia.TabIndex = 21;
            this.btnSuaTacGia.Text = "Sửa";
            this.btnSuaTacGia.UseVisualStyleBackColor = true;
            this.btnSuaTacGia.Click += new System.EventHandler(this.btnSuaTacGia_Click);
            // 
            // btnXoaTacGia
            // 
            this.btnXoaTacGia.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaTacGia.Image")));
            this.btnXoaTacGia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaTacGia.Location = new System.Drawing.Point(343, 135);
            this.btnXoaTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnXoaTacGia.Name = "btnXoaTacGia";
            this.btnXoaTacGia.Size = new System.Drawing.Size(141, 36);
            this.btnXoaTacGia.TabIndex = 20;
            this.btnXoaTacGia.Text = "Xoá";
            this.btnXoaTacGia.UseVisualStyleBackColor = true;
            this.btnXoaTacGia.Click += new System.EventHandler(this.btnXoaTacGia_Click);
            // 
            // btnThemTacGia
            // 
            this.btnThemTacGia.Image = ((System.Drawing.Image)(resources.GetObject("btnThemTacGia.Image")));
            this.btnThemTacGia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThemTacGia.Location = new System.Drawing.Point(343, 93);
            this.btnThemTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThemTacGia.Name = "btnThemTacGia";
            this.btnThemTacGia.Size = new System.Drawing.Size(141, 38);
            this.btnThemTacGia.TabIndex = 19;
            this.btnThemTacGia.Text = "Thêm";
            this.btnThemTacGia.UseVisualStyleBackColor = true;
            this.btnThemTacGia.Click += new System.EventHandler(this.btnThemTacGia_Click);
            // 
            // dgvNhapTacGia
            // 
            this.dgvNhapTacGia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNhapTacGia.Location = new System.Drawing.Point(16, 214);
            this.dgvNhapTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvNhapTacGia.Name = "dgvNhapTacGia";
            this.dgvNhapTacGia.RowHeadersWidth = 51;
            this.dgvNhapTacGia.RowTemplate.Height = 28;
            this.dgvNhapTacGia.Size = new System.Drawing.Size(684, 175);
            this.dgvNhapTacGia.TabIndex = 24;
            this.dgvNhapTacGia.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNhapTacGia_CellContentClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(212, 47);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 38);
            this.pictureBox2.TabIndex = 26;
            this.pictureBox2.TabStop = false;
            // 
            // txtTenTacGia
            // 
            this.txtTenTacGia.Location = new System.Drawing.Point(123, 131);
            this.txtTenTacGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenTacGia.Name = "txtTenTacGia";
            this.txtTenTacGia.Size = new System.Drawing.Size(167, 22);
            this.txtTenTacGia.TabIndex = 27;
            this.txtTenTacGia.TextChanged += new System.EventHandler(this.txtTenTacGia_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(711, 35);
            this.panel2.TabIndex = 57;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Tan;
            this.button5.Dock = System.Windows.Forms.DockStyle.Right;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(611, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 35);
            this.button5.TabIndex = 1;
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.IndianRed;
            this.button4.Dock = System.Windows.Forms.DockStyle.Right;
            this.button4.Location = new System.Drawing.Point(661, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 35);
            this.button4.TabIndex = 0;
            this.button4.Text = "X";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // NhapTacGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 398);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTenTacGia);
            this.Controls.Add(this.dgvNhapTacGia);
            this.Controls.Add(this.btnKhoiTaoTacGia);
            this.Controls.Add(this.btnSuaTacGia);
            this.Controls.Add(this.btnXoaTacGia);
            this.Controls.Add(this.btnThemTacGia);
            this.Controls.Add(this.txtLienLac);
            this.Controls.Add(this.txtMaTacGia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "NhapTacGia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NhapTacGia";
            this.Load += new System.EventHandler(this.NhapTacGia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapTacGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMaTacGia;
        private System.Windows.Forms.TextBox txtLienLac;
        private System.Windows.Forms.Button btnKhoiTaoTacGia;
        private System.Windows.Forms.Button btnSuaTacGia;
        private System.Windows.Forms.Button btnXoaTacGia;
        private System.Windows.Forms.Button btnThemTacGia;
        private System.Windows.Forms.DataGridView dgvNhapTacGia;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtTenTacGia;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
    }
}