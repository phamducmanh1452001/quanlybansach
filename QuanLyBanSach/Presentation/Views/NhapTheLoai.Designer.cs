﻿namespace QuanLyBanSach
{
    partial class NhapTheLoai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NhapTheLoai));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenTheLoai = new System.Windows.Forms.TextBox();
            this.dgvNhapTheLoai = new System.Windows.Forms.DataGridView();
            this.btnKhoiTaoTheLoai = new System.Windows.Forms.Button();
            this.btnSuaTheLoai = new System.Windows.Forms.Button();
            this.btnXoaTheLoai = new System.Windows.Forms.Button();
            this.btnThemTheLoai = new System.Windows.Forms.Button();
            this.txtMaTheLoai = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapTheLoai)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(220, 54);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 38);
            this.pictureBox2.TabIndex = 39;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 29);
            this.label1.TabIndex = 28;
            this.label1.Text = "Cập nhật thể loại";
            // 
            // txtTenTheLoai
            // 
            this.txtTenTheLoai.Location = new System.Drawing.Point(123, 138);
            this.txtTenTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenTheLoai.Name = "txtTenTheLoai";
            this.txtTenTheLoai.Size = new System.Drawing.Size(167, 22);
            this.txtTenTheLoai.TabIndex = 40;
            // 
            // dgvNhapTheLoai
            // 
            this.dgvNhapTheLoai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNhapTheLoai.Location = new System.Drawing.Point(16, 190);
            this.dgvNhapTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvNhapTheLoai.Name = "dgvNhapTheLoai";
            this.dgvNhapTheLoai.RowHeadersWidth = 51;
            this.dgvNhapTheLoai.RowTemplate.Height = 28;
            this.dgvNhapTheLoai.Size = new System.Drawing.Size(684, 195);
            this.dgvNhapTheLoai.TabIndex = 38;
            this.dgvNhapTheLoai.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNhapTheLoai_CellContentClick);
            // 
            // btnKhoiTaoTheLoai
            // 
            this.btnKhoiTaoTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoiTaoTheLoai.Image")));
            this.btnKhoiTaoTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKhoiTaoTheLoai.Location = new System.Drawing.Point(473, 131);
            this.btnKhoiTaoTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnKhoiTaoTheLoai.Name = "btnKhoiTaoTheLoai";
            this.btnKhoiTaoTheLoai.Size = new System.Drawing.Size(104, 32);
            this.btnKhoiTaoTheLoai.TabIndex = 37;
            this.btnKhoiTaoTheLoai.Text = "Clear";
            this.btnKhoiTaoTheLoai.UseVisualStyleBackColor = true;
            this.btnKhoiTaoTheLoai.Click += new System.EventHandler(this.btnKhoiTaoTheLoai_Click);
            // 
            // btnSuaTheLoai
            // 
            this.btnSuaTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaTheLoai.Image")));
            this.btnSuaTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSuaTheLoai.Location = new System.Drawing.Point(473, 95);
            this.btnSuaTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSuaTheLoai.Name = "btnSuaTheLoai";
            this.btnSuaTheLoai.Size = new System.Drawing.Size(104, 31);
            this.btnSuaTheLoai.TabIndex = 36;
            this.btnSuaTheLoai.Text = "Sửa";
            this.btnSuaTheLoai.UseVisualStyleBackColor = true;
            this.btnSuaTheLoai.Click += new System.EventHandler(this.btnSuaTheLoai_Click);
            // 
            // btnXoaTheLoai
            // 
            this.btnXoaTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaTheLoai.Image")));
            this.btnXoaTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaTheLoai.Location = new System.Drawing.Point(343, 131);
            this.btnXoaTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnXoaTheLoai.Name = "btnXoaTheLoai";
            this.btnXoaTheLoai.Size = new System.Drawing.Size(113, 32);
            this.btnXoaTheLoai.TabIndex = 35;
            this.btnXoaTheLoai.Text = "Xoá";
            this.btnXoaTheLoai.UseVisualStyleBackColor = true;
            this.btnXoaTheLoai.Click += new System.EventHandler(this.btnXoaTheLoai_Click);
            // 
            // btnThemTheLoai
            // 
            this.btnThemTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnThemTheLoai.Image")));
            this.btnThemTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThemTheLoai.Location = new System.Drawing.Point(343, 95);
            this.btnThemTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThemTheLoai.Name = "btnThemTheLoai";
            this.btnThemTheLoai.Size = new System.Drawing.Size(113, 31);
            this.btnThemTheLoai.TabIndex = 34;
            this.btnThemTheLoai.Text = "Thêm";
            this.btnThemTheLoai.UseVisualStyleBackColor = true;
            this.btnThemTheLoai.Click += new System.EventHandler(this.btnThemTheLoai_Click);
            // 
            // txtMaTheLoai
            // 
            this.txtMaTheLoai.Location = new System.Drawing.Point(123, 100);
            this.txtMaTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaTheLoai.Name = "txtMaTheLoai";
            this.txtMaTheLoai.Size = new System.Drawing.Size(167, 22);
            this.txtMaTheLoai.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 30;
            this.label3.Text = "Tên thể loại";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Mã thể loại";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(711, 35);
            this.panel2.TabIndex = 57;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Tan;
            this.button5.Dock = System.Windows.Forms.DockStyle.Right;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(611, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 35);
            this.button5.TabIndex = 1;
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.IndianRed;
            this.button4.Dock = System.Windows.Forms.DockStyle.Right;
            this.button4.Location = new System.Drawing.Point(661, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 35);
            this.button4.TabIndex = 0;
            this.button4.Text = "X";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // NhapTheLoai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 405);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTenTheLoai);
            this.Controls.Add(this.dgvNhapTheLoai);
            this.Controls.Add(this.btnKhoiTaoTheLoai);
            this.Controls.Add(this.btnSuaTheLoai);
            this.Controls.Add(this.btnXoaTheLoai);
            this.Controls.Add(this.btnThemTheLoai);
            this.Controls.Add(this.txtMaTheLoai);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "NhapTheLoai";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NhapTheLoai";
            this.Load += new System.EventHandler(this.NhapTheLoai_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapTheLoai)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenTheLoai;
        private System.Windows.Forms.DataGridView dgvNhapTheLoai;
        private System.Windows.Forms.Button btnKhoiTaoTheLoai;
        private System.Windows.Forms.Button btnSuaTheLoai;
        private System.Windows.Forms.Button btnXoaTheLoai;
        private System.Windows.Forms.Button btnThemTheLoai;
        private System.Windows.Forms.TextBox txtMaTheLoai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
    }
}