﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach
{
    public partial class NhapNhaXuatBan : Form
    {
        private CapNhatNhaXuatBanViewModel capNhatNhaXuatBanViewModel;
        private TimKiemNhaXuatBanViewModel timKiemNhaXuatBanViewModel;

        private void loadData()
        {
            DataTable dt = timKiemNhaXuatBanViewModel.LayTatCa();
            dgvNhapNXB.DataSource = dt;
        }
        public NhapNhaXuatBan()
        {
            InitializeComponent();
            capNhatNhaXuatBanViewModel = new CapNhatNhaXuatBanViewModel();
            timKiemNhaXuatBanViewModel = new TimKiemNhaXuatBanViewModel();
        }

        private void btnThemTheLoai_Click(object sender, EventArgs e)
        {

        }

        private void NhapNhaXuatBan_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void dgvNhapNXB_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = dgvNhapNXB.CurrentRow.Index;
            txtMaNXB.Text = dgvNhapNXB.Rows[i].Cells[0].Value.ToString();
            txtTenNXB.Text = dgvNhapNXB.Rows[i].Cells[1].Value.ToString();
            txtDiaChiNXB.Text = dgvNhapNXB.Rows[i].Cells[2].Value.ToString();
            txtDienThoaiNXB.Text = dgvNhapNXB.Rows[i].Cells[3].Value.ToString();
        }

        private void btnXoaTheLoai_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn xoá không?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                capNhatNhaXuatBanViewModel.XoaNhaXuatBanTheoMa(txtMaNXB.Text);
                loadData();
            }

        }

        private void btnSuaTheLoai_Click(object sender, EventArgs e)
        {
            string maNXB = txtMaNXB.Text;
            var nxb = new NhaXuatBan();
            nxb.TenNXB = txtTenNXB.Text;
            capNhatNhaXuatBanViewModel.CapNhatNhaXuatBanTheoMa(maNXB, nxb);
            loadData();
        }

        private void btnKhoiTaoTheLoai_Click(object sender, EventArgs e)
        {
            txtMaNXB.Clear();
            txtTenNXB.Clear();
        }

        private void btnThemNXB_Click(object sender, EventArgs e)
        {
            if (txtMaNXB.Text == "" || txtTenNXB.Text == "" || txtDiaChiNXB.Text == ""||txtDienThoaiNXB.Text=="")
            {
                MessageBox.Show("Yêu cầu nhập đầy đủ dữ liệu");
                txtMaNXB.Clear();
                txtTenNXB.Clear();
                txtDiaChiNXB.Clear();
                txtDienThoaiNXB.Clear();
                txtMaNXB.Focus();
            }
            else
            {
                string maNXB = txtMaNXB.Text;
                var nxb = new NhaXuatBan();
                nxb.MaNXB = maNXB;
                nxb.TenNXB = txtTenNXB.Text;
                nxb.DienThoai = txtDienThoaiNXB.Text;
                nxb.DiaChi = txtDiaChiNXB.Text;
                capNhatNhaXuatBanViewModel.NhapNhaXuatBan(nxb);
                loadData();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
