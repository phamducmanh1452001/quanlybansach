﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach
{
    public partial class NhapTacGia : Form
    {
        private CapNhatTacGiaViewModel capNhatTacGiaViewModel;
        private TimKiemTacGiaViewModel timKiemTacGiaViewModel;

        private void loadData()
        {
            var dt = timKiemTacGiaViewModel.LayTatCa();
            dgvNhapTacGia.DataSource = dt;
        }
        public NhapTacGia()
        {
            InitializeComponent();
            capNhatTacGiaViewModel=  new CapNhatTacGiaViewModel();
            timKiemTacGiaViewModel = new TimKiemTacGiaViewModel();
        }

        private void txtMaTacGia_TextChanged(object sender, EventArgs e)
        {

        }

        private void NhapTacGia_Load(object sender, EventArgs e)
        {
            loadData();

        }

        private void btnThemTacGia_Click(object sender, EventArgs e)
        {

            if (txtMaTacGia.Text == "" || txtTenTacGia.Text == "" || txtLienLac.Text == "")
            {
                MessageBox.Show("Yêu cầu nhập đầy đủ dữ liệu");
                txtMaTacGia.Clear();
                txtTenTacGia.Clear();
                txtLienLac.Clear();
                txtMaTacGia.Focus();
            }
            else
            {
                var tacGia = new TacGia();
                tacGia.MaTacGia = txtMaTacGia.Text;
                tacGia.TenTacGia = txtTenTacGia.Text;
                tacGia.LienLac = txtLienLac.Text;

                capNhatTacGiaViewModel.NhapTacGia(tacGia);

                loadData();

            }


        }

        private void dgvNhapTacGia_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int i;
            i=dgvNhapTacGia.CurrentRow.Index;
            txtMaTacGia.Text = dgvNhapTacGia.Rows[i].Cells[0].Value.ToString();
            txtTenTacGia.Text= dgvNhapTacGia.Rows[i].Cells[1].Value.ToString();
            txtLienLac.Text= dgvNhapTacGia.Rows[i].Cells[2].Value.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaTacGia_Click(object sender, EventArgs e)
        {
            DialogResult result= MessageBox.Show("Bạn có chắc muốn xoá không?","Cảnh báo",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if(result==DialogResult.Yes)
            {
                capNhatTacGiaViewModel.XoaTacGiaTheoMa(txtMaTacGia.Text);
                loadData();
            }
            

        }

        private void btnSuaTacGia_Click(object sender, EventArgs e)
        {
            var tacGia = new TacGia();
            tacGia.MaTacGia = txtMaTacGia.Text;
            tacGia.TenTacGia = txtTenTacGia.Text;
            tacGia.LienLac = txtLienLac.Text;

            capNhatTacGiaViewModel.CapNhatTacGiaTheoMa(txtMaTacGia.Text, tacGia);

            loadData();

        }

        private void btnKhoiTaoTacGia_Click(object sender, EventArgs e)
        {
            txtMaTacGia.Text = "";
            txtTenTacGia.Text = "";
            txtLienLac.Text = "";
        }

        private void txtTenTacGia_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
