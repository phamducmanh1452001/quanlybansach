﻿using System;
using System.Windows.Forms;

namespace QuanLyBanSach
{
    public partial class Main : Form
    {
        bool homecollapse;
        bool sidebarexpand;
        bool updatecollapse;
        public Main()
        {
            InitializeComponent();
        }
        void PhanQuyen()
        {
            if(Const.LoaiTaiKhoan==false)
            {
                button10.Enabled = false;
                button9.Enabled = false;
            }
            else
            {
                button10.Enabled = true;
                button9.Enabled = true;
            }
        }
        private void Main_Load(object sender, EventArgs e)
        {
            PhanQuyen();
        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult lt;
            lt = MessageBox.Show("Thoat?", "Kết thúc", MessageBoxButtons.YesNo);
            if(lt==DialogResult.Yes)
            {
                this.Close();
            }
        }    
        
        private void sidebarTimer_Tick(object sender, EventArgs e)
        {


            if(sidebarexpand)
            {
                sidebar.Width -= 10;
                if(sidebar.Width==sidebar.MinimumSize.Width)
                {
                    sidebarexpand = false;
                    sidebarTimer.Stop();
                }
            }
            else
            {
                sidebar.Width += 10;
                if(sidebar.Width==sidebar.MaximumSize.Width)
                {
                    sidebarexpand = true;
                    sidebarTimer.Stop();
                }
            }
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            sidebarTimer.Start();
        }

        private void homeTimer_Tick(object sender, EventArgs e)
        {
            if(homecollapse)
            {
                SearchContainer.Height += 10;
                if(SearchContainer.Height==SearchContainer.MaximumSize.Height)
                {
                    homecollapse = false;
                    homeTimer.Stop();

                }
            }
            else
            {
                SearchContainer.Height -= 10;
                if(SearchContainer.Height==SearchContainer.MinimumSize.Height)
                {
                    homecollapse = true;
                    homeTimer.Stop();
                }
            }
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            homeTimer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TimKiemTacGia frm = new TimKiemTacGia();
            frm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TimKiemNXB timKiemNXB = new TimKiemNXB();
            timKiemNXB.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TimKiemTheLoai timKiemTheLoai = new TimKiemTheLoai();
            timKiemTheLoai.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized; 
        }

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            if (updatecollapse)
            {
                UpdateContainer.Height += 10;
                if (UpdateContainer.Height == UpdateContainer.MaximumSize.Height)
                {
                    updatecollapse = false;
                    UpdateTimer.Stop();

                }
            }
            else
            {
                UpdateContainer.Height -= 10;
                if (UpdateContainer.Height == UpdateContainer.MinimumSize.Height)
                {
                    updatecollapse = true;
                    UpdateTimer.Stop();
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            UpdateTimer.Start();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            NhapSach nhapSach = new NhapSach();
            nhapSach.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            HoaDonBanSach hoaDonBanSach = new HoaDonBanSach();
            hoaDonBanSach.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            NhapTacGia nhapTacGia = new NhapTacGia();
            nhapTacGia.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            NhapNhaXuatBan nhapNhaXuatBan = new NhapNhaXuatBan();
            nhapNhaXuatBan.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            NhapTheLoai nhapTheLoai = new NhapTheLoai();
            nhapTheLoai.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            MessageBox.Show("BOOK HOUSE XIN KÍNH CHÚC QUÝ KHÁCH HÀNG MỘT NGÀY TỐT LÀNH, BOOK HOUSE LÀ HỆ THỐNG BÁN SÁCH CHÍNH HÃNG VỚI NHIỀU MẪU SÁCH ĐA DẠNG PHÙ HỢP VỚI MỌI LỨA TUỔI, HIỆN TẠI BOOK HOUSE ĐÃ CÓ MẶT TẠI CÁC CƠ SỞ 45 PHƯƠNG MAI, 22 TÔN THẤT TÙNG, 67 CẦU GIẦY NẾU BẠN CÓ THẮC MẮC XIN HÃY LIÊN HỆ VỚI CHÚNG TÔI QUA SỐ ĐIỆN THOẠI 0921267324 XIN CHÂN THÀNH CẢM ƠN!","Giới thiệu",MessageBoxButtons.OK,MessageBoxIcon.None);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
