﻿namespace QuanLyBanSach
{
    partial class NhapNhaXuatBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NhapNhaXuatBan));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenNXB = new System.Windows.Forms.TextBox();
            this.dgvNhapNXB = new System.Windows.Forms.DataGridView();
            this.btnKhoiTaoTheLoai = new System.Windows.Forms.Button();
            this.btnSuaTheLoai = new System.Windows.Forms.Button();
            this.btnXoaTheLoai = new System.Windows.Forms.Button();
            this.btnThemTheLoai = new System.Windows.Forms.Button();
            this.txtMaNXB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiaChiNXB = new System.Windows.Forms.TextBox();
            this.txtDienThoaiNXB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapNXB)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(284, 61);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 38);
            this.pictureBox2.TabIndex = 50;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(248, 29);
            this.label1.TabIndex = 41;
            this.label1.Text = "Cập nhật nhà xuất bản";
            // 
            // txtTenNXB
            // 
            this.txtTenNXB.Location = new System.Drawing.Point(123, 142);
            this.txtTenNXB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenNXB.Name = "txtTenNXB";
            this.txtTenNXB.Size = new System.Drawing.Size(167, 22);
            this.txtTenNXB.TabIndex = 51;
            // 
            // dgvNhapNXB
            // 
            this.dgvNhapNXB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNhapNXB.Location = new System.Drawing.Point(16, 258);
            this.dgvNhapNXB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvNhapNXB.Name = "dgvNhapNXB";
            this.dgvNhapNXB.RowHeadersWidth = 51;
            this.dgvNhapNXB.RowTemplate.Height = 28;
            this.dgvNhapNXB.Size = new System.Drawing.Size(684, 179);
            this.dgvNhapNXB.TabIndex = 49;
            this.dgvNhapNXB.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNhapNXB_CellContentClick);
            // 
            // btnKhoiTaoTheLoai
            // 
            this.btnKhoiTaoTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoiTaoTheLoai.Image")));
            this.btnKhoiTaoTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKhoiTaoTheLoai.Location = new System.Drawing.Point(516, 154);
            this.btnKhoiTaoTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnKhoiTaoTheLoai.Name = "btnKhoiTaoTheLoai";
            this.btnKhoiTaoTheLoai.Size = new System.Drawing.Size(112, 36);
            this.btnKhoiTaoTheLoai.TabIndex = 48;
            this.btnKhoiTaoTheLoai.Text = "Clear";
            this.btnKhoiTaoTheLoai.UseVisualStyleBackColor = true;
            this.btnKhoiTaoTheLoai.Click += new System.EventHandler(this.btnKhoiTaoTheLoai_Click);
            // 
            // btnSuaTheLoai
            // 
            this.btnSuaTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnSuaTheLoai.Image")));
            this.btnSuaTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSuaTheLoai.Location = new System.Drawing.Point(516, 103);
            this.btnSuaTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSuaTheLoai.Name = "btnSuaTheLoai";
            this.btnSuaTheLoai.Size = new System.Drawing.Size(112, 36);
            this.btnSuaTheLoai.TabIndex = 47;
            this.btnSuaTheLoai.Text = "Sửa";
            this.btnSuaTheLoai.UseVisualStyleBackColor = true;
            this.btnSuaTheLoai.Click += new System.EventHandler(this.btnSuaTheLoai_Click);
            // 
            // btnXoaTheLoai
            // 
            this.btnXoaTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaTheLoai.Image")));
            this.btnXoaTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaTheLoai.Location = new System.Drawing.Point(380, 103);
            this.btnXoaTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnXoaTheLoai.Name = "btnXoaTheLoai";
            this.btnXoaTheLoai.Size = new System.Drawing.Size(112, 36);
            this.btnXoaTheLoai.TabIndex = 46;
            this.btnXoaTheLoai.Text = "Xoá";
            this.btnXoaTheLoai.UseVisualStyleBackColor = true;
            this.btnXoaTheLoai.Click += new System.EventHandler(this.btnXoaTheLoai_Click);
            // 
            // btnThemTheLoai
            // 
            this.btnThemTheLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnThemTheLoai.Image")));
            this.btnThemTheLoai.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThemTheLoai.Location = new System.Drawing.Point(380, 154);
            this.btnThemTheLoai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThemTheLoai.Name = "btnThemTheLoai";
            this.btnThemTheLoai.Size = new System.Drawing.Size(112, 36);
            this.btnThemTheLoai.TabIndex = 45;
            this.btnThemTheLoai.Text = "Thêm";
            this.btnThemTheLoai.UseVisualStyleBackColor = true;
            this.btnThemTheLoai.Click += new System.EventHandler(this.btnThemNXB_Click);
            // 
            // txtMaNXB
            // 
            this.txtMaNXB.Location = new System.Drawing.Point(123, 103);
            this.txtMaNXB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaNXB.Name = "txtMaNXB";
            this.txtMaNXB.Size = new System.Drawing.Size(167, 22);
            this.txtMaNXB.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.TabIndex = 43;
            this.label3.Text = "Tên NXB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "Mã NXB";
            // 
            // txtDiaChiNXB
            // 
            this.txtDiaChiNXB.Location = new System.Drawing.Point(123, 182);
            this.txtDiaChiNXB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDiaChiNXB.Name = "txtDiaChiNXB";
            this.txtDiaChiNXB.Size = new System.Drawing.Size(167, 22);
            this.txtDiaChiNXB.TabIndex = 52;
            // 
            // txtDienThoaiNXB
            // 
            this.txtDienThoaiNXB.Location = new System.Drawing.Point(123, 221);
            this.txtDienThoaiNXB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDienThoaiNXB.Name = "txtDienThoaiNXB";
            this.txtDienThoaiNXB.Size = new System.Drawing.Size(167, 22);
            this.txtDienThoaiNXB.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 54;
            this.label4.Text = "Địa chỉ NXB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 16);
            this.label5.TabIndex = 55;
            this.label5.Text = "Điện thoại";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(711, 35);
            this.panel2.TabIndex = 56;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Tan;
            this.button5.Dock = System.Windows.Forms.DockStyle.Right;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(611, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 35);
            this.button5.TabIndex = 1;
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.IndianRed;
            this.button4.Dock = System.Windows.Forms.DockStyle.Right;
            this.button4.Location = new System.Drawing.Point(661, 0);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 35);
            this.button4.TabIndex = 0;
            this.button4.Text = "X";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // NhapNhaXuatBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 446);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDienThoaiNXB);
            this.Controls.Add(this.txtDiaChiNXB);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTenNXB);
            this.Controls.Add(this.dgvNhapNXB);
            this.Controls.Add(this.btnKhoiTaoTheLoai);
            this.Controls.Add(this.btnSuaTheLoai);
            this.Controls.Add(this.btnXoaTheLoai);
            this.Controls.Add(this.btnThemTheLoai);
            this.Controls.Add(this.txtMaNXB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "NhapNhaXuatBan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NhapNhaXuatBan";
            this.Load += new System.EventHandler(this.NhapNhaXuatBan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapNXB)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenNXB;
        private System.Windows.Forms.DataGridView dgvNhapNXB;
        private System.Windows.Forms.Button btnKhoiTaoTheLoai;
        private System.Windows.Forms.Button btnSuaTheLoai;
        private System.Windows.Forms.Button btnXoaTheLoai;
        private System.Windows.Forms.Button btnThemTheLoai;
        private System.Windows.Forms.TextBox txtMaNXB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDiaChiNXB;
        private System.Windows.Forms.TextBox txtDienThoaiNXB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
    }
}