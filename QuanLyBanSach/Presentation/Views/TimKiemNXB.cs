﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;

namespace QuanLyBanSach
{
    public partial class TimKiemNXB : Form
    {
        private TimKiemNhaXuatBanViewModel timKiemNhaXuatBanViewModel;
        private TimKiemSachViewModel timKiemSachViewModel;

        public TimKiemNXB()
        {
            InitializeComponent();
            timKiemNhaXuatBanViewModel = new TimKiemNhaXuatBanViewModel();
            timKiemSachViewModel = new TimKiemSachViewModel();
        }


        private void cboChonNXB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TimKiemNXB_Load(object sender, EventArgs e)
        {
            DataTable tableNS = timKiemNhaXuatBanViewModel.LayTatCa();
            cboChonNXB.DataSource = tableNS;
            cboChonNXB.DisplayMember = "TenNXB";
            cboChonNXB.ValueMember = "TenNXB";

            if (cboChonNXB.Items.Count > 1)
            {
                cboChonNXB.SelectedIndex = -1;
            }
        }

        private void btnThoatNXB_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnTimNXB_Click(object sender, EventArgs e)
        {
            string maNXB = cboChonNXB.SelectedValue.ToString();
            DataTable dt = timKiemSachViewModel.TimTheoNhaXuatBan(maNXB);
            dgvkqNXB.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
