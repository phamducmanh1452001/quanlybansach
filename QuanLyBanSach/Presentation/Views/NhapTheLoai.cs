﻿using System;
using System.Windows.Forms;
using QuanLyBanSach.Presentation.ViewModels;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach
{
    public partial class NhapTheLoai : Form
    {
        CapNhatTheLoaiViewModel capNhatTheLoaiViewModel;
        TimKiemTheLoaiViewModel timKiemTheLoaiViewModel;
        
        void loadData()
        {
            var dt = timKiemTheLoaiViewModel.LayTatCa();
            dgvNhapTheLoai.DataSource = dt;
        }
        public NhapTheLoai()
        {
            InitializeComponent();
            capNhatTheLoaiViewModel = new CapNhatTheLoaiViewModel();
            timKiemTheLoaiViewModel = new TimKiemTheLoaiViewModel();
        }

        private void btnThemTheLoai_Click(object sender, EventArgs e)
        {
            if (txtMaTheLoai.Text == "" || txtTenTheLoai.Text == "")
            {
                MessageBox.Show("Yêu cầu nhập đầy đủ dữ liệu");
                txtMaTheLoai.Clear();
                txtTenTheLoai.Clear();
                txtMaTheLoai.Focus();
            }
            else
            {
                var theLoai = new TheLoai();
                theLoai.MaTL = txtMaTheLoai.Text;
                theLoai.TenTL = txtTenTheLoai.Text;
                capNhatTheLoaiViewModel.NhapTheLoai(theLoai);
                loadData();
            }
        }

        private void NhapTheLoai_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void btnXoaTheLoai_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn xoá không?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                capNhatTheLoaiViewModel.XoaTheLoaiTheoMa(txtMaTheLoai.Text);
                loadData();
            }
           
        }

        private void dgvNhapTheLoai_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = dgvNhapTheLoai.CurrentRow.Index;
            txtMaTheLoai.Text = dgvNhapTheLoai.Rows[i].Cells[0].Value.ToString();
            txtTenTheLoai.Text = dgvNhapTheLoai.Rows[i].Cells[1].Value.ToString();

            
        }

        private void btnSuaTheLoai_Click(object sender, EventArgs e)
        {
            string maTL = txtMaTheLoai.Text;
            var theLoai = new TheLoai();
            theLoai.MaTL = maTL;
            theLoai.TenTL = txtTenTheLoai.Text;

            capNhatTheLoaiViewModel.CapNhatTheLoaiTheoMa(maTL, theLoai);
            loadData();
        }

        private void btnKhoiTaoTheLoai_Click(object sender, EventArgs e)
        {
            txtMaTheLoai.Clear();
            txtTenTheLoai.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
