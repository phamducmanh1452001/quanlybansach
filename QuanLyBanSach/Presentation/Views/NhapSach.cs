﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyBanSach.Presentation.ViewModels;
using QuanLyBanSach.Domain;

namespace QuanLyBanSach
{
    public partial class NhapSach : Form

    {
        private CapNhatSachViewModel capNhatSachViewModel;
        private TimKiemSachViewModel timKiemSachViewModel;
        private TimKiemTacGiaViewModel timKiemTacGiaViewModel;
        private TimKiemTheLoaiViewModel timKiemTheLoaiViewModel;
        private TimKiemNhaXuatBanViewModel timKiemNhaXuatBanViewModel;

        public NhapSach()
        {
            InitializeComponent();
            capNhatSachViewModel = new CapNhatSachViewModel();
            timKiemSachViewModel = new TimKiemSachViewModel();
            timKiemTacGiaViewModel = new TimKiemTacGiaViewModel();
            timKiemTheLoaiViewModel = new TimKiemTheLoaiViewModel();
            timKiemNhaXuatBanViewModel = new TimKiemNhaXuatBanViewModel();
        }

        private void loadData()
        {
            var dt = timKiemSachViewModel.LayTatCaSach();
            dgvPhieuNhapSach.DataSource = dt;
        }
        
       
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn xoá không?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                capNhatSachViewModel.XoaSachTheoMa(txtMaSach.Text);
                loadData();
            }
        }

        private void NhapSach_Load(object sender, EventArgs e)
        {
            var tacGiaDt = timKiemTacGiaViewModel.LayTatCa();
            
            cboMaTacGia.DisplayMember = "TenTG";    
            cboMaTacGia.ValueMember= "TenTG";

            cboMaTacGia.DataSource = tacGiaDt;

            if (cboMaTacGia.Items.Count>1)
            {
                cboMaTacGia.SelectedIndex = -1;
            }

            //Combo box Nhập thể loại
            //
            var theLoaiDt = timKiemTheLoaiViewModel.LayTatCa();
            cboMaTheLoai.DisplayMember = "TenTL";
            cboMaTheLoai.ValueMember = "MaTL";
            cboMaTheLoai.DataSource = theLoaiDt;

            if (cboMaTheLoai.Items.Count > 1)
            {
                cboMaTheLoai.SelectedIndex = -1;
            }


            //Combo box Nhập nhà xuất bản
            //
            var nxbDt = timKiemNhaXuatBanViewModel.LayTatCa();            
            cboNhaXuatBan.DisplayMember = "TenNXB";
            cboNhaXuatBan.ValueMember = "MaNXB";
            cboNhaXuatBan.DataSource = nxbDt;

            if (cboNhaXuatBan.Items.Count > 1)
            {
                cboNhaXuatBan.SelectedIndex = -1;
            }

            loadData();
        }

        private void btnThemSach_Click(object sender, EventArgs e)
        {
            if (txtMaSach.Text == "" || txtGiaBan.Text == "" || txtGiaNhap.Text == "" || txtTenSach.Text == "" || txtSoLuong.Value == 0 || cboMaTacGia.SelectedIndex ==-1 || cboMaTheLoai.SelectedIndex ==-1 || cboNhaXuatBan.SelectedIndex==-1)
            {
                MessageBox.Show("Yêu cầu nhập đầy đủ thông tin");
                txtMaSach.Focus();
            }
            else
            {
                string MaTG = cboMaTacGia.SelectedValue.ToString();
                string MaTL = cboMaTheLoai.SelectedValue.ToString();
                string MaNXB = cboNhaXuatBan.SelectedValue.ToString();
                string MaSach = txtMaSach.Text.ToString();

                var sach = new Sach();
                sach.MaSach = MaSach;
                sach.MaTG = MaTG;
                sach.MaTL = MaTL;
                sach.MaNXB = MaNXB;

                capNhatSachViewModel.NhapSach(sach);
                loadData();
            }
        }

        private void cboTheLoai_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboTacGia_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnKhoiTaoSach_Click(object sender, EventArgs e)
        {
            txtGiaNhap.Clear();
            txtMaSach.Clear();
            txtSoLuong.Value = 0;
            txtTenSach.Clear();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = dgvPhieuNhapSach.CurrentRow.Index;
            txtMaSach.Text = dgvPhieuNhapSach.Rows[i].Cells[0].Value.ToString();
            txtTenSach.Text = dgvPhieuNhapSach.Rows[i].Cells[1].Value.ToString();
            cboMaTheLoai.DisplayMember = dgvPhieuNhapSach.Rows[i].Cells[2].Value.ToString();
            cboMaTacGia.DisplayMember = dgvPhieuNhapSach.Rows[i].Cells[3].Value.ToString();
            cboNhaXuatBan.DisplayMember = dgvPhieuNhapSach.Rows[i].Cells[4].Value.ToString();
            txtGiaNhap.Text = dgvPhieuNhapSach.Rows[i].Cells[6].Value.ToString();
            txtGiaBan.Text = dgvPhieuNhapSach.Rows[i].Cells[7].Value.ToString();
        }

        private void KhoiTao_Click(object sender, EventArgs e)
        {
            txtGiaNhap.Clear();
            txtMaSach.Clear();
            txtSoLuong.Value = 0;
            txtTenSach.Clear();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string maSach = txtMaSach.Text;
            var sach = new Sach();
            sach.TenSach = txtTenSach.Text;
            sach.SoLuongTon = Convert.ToInt32(txtSoLuong.Value);
            sach.MaSach = maSach;

            capNhatSachViewModel.CapNhatSachTheoMa(maSach, sach);
        }

        private void cboNhaXuatBan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
