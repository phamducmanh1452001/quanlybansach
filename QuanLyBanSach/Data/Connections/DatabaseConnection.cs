﻿
using System.Data.SqlClient;
using System.Data;
using System;

namespace QuanLyBanSach.Data.Connections
{
    public class DatabaseConnection
    {
        public static string source;
        public static SqlConnection con = new SqlConnection();

        static DatabaseConnection()
        {
            source = @"server=.\SQLEXPRESS;Initial Catalog=QLBS;Persist Security Info=True;User ID=sa;Password=13579";
            con = new SqlConnection(source);
            if (con.State == ConnectionState.Open)
            {
                con.Open();
            }
            else if (con.State == ConnectionState.Closed)
            {
                con.Close();
            }
        }
        public static void open()
        {
            try
            {
                con.Open();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Dispose();
            }
        }
        public static DataTable DataTable_Sql(string sql)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(source))
                {
                    using (SqlDataAdapter dap = new SqlDataAdapter(sql, conn))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            dap.Fill(ds);
                            conn.Close();
                            conn.Dispose();
                            return ds.Tables[0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static int Execute_NonSQL(string sql)
        {
            SqlConnection conn = new SqlConnection(source);
            SqlCommand cmd = new SqlCommand(sql, conn);
            int row = 0;
            conn.Open();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            row = cmd.ExecuteNonQuery();
            conn.Dispose();
            conn.Close();
            return row;
        }

    }
}
