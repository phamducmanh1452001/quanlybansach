﻿using System;
using System.Collections.Generic;
using System.Data;
using QuanLyBanSach.Data.Connections;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Data.Repositories
{
    public class NhaXuatBanRepositoryImpl : NhaXuatBanRepository
    {
        public NhaXuatBanRepositoryImpl() { }

        bool NhaXuatBanRepository.NhapNhaXuatBan(NhaXuatBan nxb)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Insert into NhaXuatBan values(N'" + nxb.MaNXB + "',N'" + nxb.TenNXB + "',N'" + nxb.DiaChi + "',N'" + nxb.DienThoai + "')");
                return true;
            }
            catch
            {
                return false;
            }
        }


        List<NhaXuatBan> NhaXuatBanRepository.LayTatCa() {
            var dt = DatabaseConnection.DataTable_Sql("Select MaNXB,TenNXB,DiaChiNXB,DienThoai from NhaXuatBan");
            return ConvertDataTableToList(dt);
        }

        List<NhaXuatBan> NhaXuatBanRepository.TimKiemNhaXuatBanTheoMa(string maNXB)
        {
            var dt = DatabaseConnection.DataTable_Sql("Select MaNXB,TenNXB,DiaChiNXB,DienThoai from NhaXuatBan Where MaNXB=" + maNXB);
            return ConvertDataTableToList(dt);
        }

        private List<NhaXuatBan> ConvertDataTableToList(DataTable dataTable)
        {
            List<NhaXuatBan> nxbList = new List<NhaXuatBan>();

            foreach (DataRow row in dataTable.Rows)
            { 
                NhaXuatBan nxb = new NhaXuatBan();
                nxb.MaNXB = row["MaNXB"].ToString();
                nxb.TenNXB = row["TenNXB"].ToString();
                nxb.DiaChi = row["DiaChiNXB"].ToString();
                nxb.DienThoai = row["DienThoai"].ToString();
                // ...

                nxbList.Add(nxb);
            }

            return nxbList;
        }

        bool NhaXuatBanRepository.XoaNhaXuatBanTheoMa(string maNXB)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Delete from NhaXuatBan where MaNXB='" + maNXB + "'");
                return true;
            }
            catch
            {
                return false;
            }
        }

        bool NhaXuatBanRepository.CapNhatNhaXuatBanTheoMa(string maNXB, NhaXuatBan nxb)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("update NhaXuatBan set TenNXB='" + nxb.TenNXB + "'where MaNXB='" + maNXB + "'");
                return true;
            } catch
            {
                return false;
            }
            
        }

        
    }
}
