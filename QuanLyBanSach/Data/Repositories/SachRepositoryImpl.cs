﻿using System;
using System.Collections.Generic;
using System.Data;
using QuanLyBanSach.Data.Connections;
using QuanLyBanSach.Domain;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Data.Repositories
{
    public class SachRepositoryImpl : SachRepository
    {
        public SachRepositoryImpl() { }
        bool SachRepository.NhapSach(Sach sach)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Insert into Sach values(N'" + sach.MaSach + "',N'" + sach.TenSach + "',N'" + sach.MaTL + "',N'" + sach.MaTG + "',N'" + sach.MaNXB + "'," + sach.SoLuongTon + ",N'" + sach.GiaNhap + "',N'" + sach.GiaBan + "')");
                return true;
            } catch
            {
                return false;
            }
        }

        bool SachRepository.XoaSachTheoMa(string maSach)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Delete from Sach where MaSach='" + maSach + "'");
                return true;
            } catch
            {
                return false;
            }
        }

        bool SachRepository.CapNhatSachTheoMa(string maSach, Sach sach)
        {
            try
            {
                var _ = DatabaseConnection
                    .DataTable_Sql("update Sach set TenSach=N'" + sach.TenSach + "',SoLuongTon=N'" + sach.SoLuongTon + "' where MaSach=N'" + maSach  + "'");
                return true;
            }
            catch
            {
                return false;
            }
        }

        List<Sach> SachRepository.TimKiemSachTheoNhaXuatBan(string maNXB)
        {
            var dt = DatabaseConnection.DataTable_Sql("Select * from Sach where MaNXB='" + maNXB + "'");
            return ConvertDataTableToList(dt);
        }

        List<Sach> SachRepository.TimKiemSachTheoTacGia(string maTG)
        {
            var dt = DatabaseConnection.DataTable_Sql("select * from Sach where MaTG='" + maTG + "'");
            return ConvertDataTableToList(dt);
        }

        List<Sach> SachRepository.TimKiemSachTheoTheLoai(string maTL)
        {

            var dt = DatabaseConnection.DataTable_Sql("Select * from Sach where MaTL = '" + maTL + "'");
            return ConvertDataTableToList(dt);
        }

        List<Sach> SachRepository.LayTatCa()
        {
            var dt = DatabaseConnection.DataTable_Sql("Select * from Sach");
            return ConvertDataTableToList(dt);
        }

        private List<Sach> ConvertDataTableToList(DataTable dataTable)
        {
            List<Sach> bookList = new List<Sach>();

            foreach (DataRow row in dataTable.Rows)
            {
                Sach sach = new Sach();
                sach.MaSach = row["MaSach"].ToString();
                sach.TenSach = row["TenSach"].ToString();
                sach.MaTL = row["MaTL"].ToString();
                sach.MaTG = row["MaTG"].ToString();
                sach.MaNXB = row["MaNXB"].ToString();
                sach.SoLuongTon = Convert.ToInt32(row["SoLuongTon"]);
                sach.GiaNhap = Convert.ToInt32(row["GiaNhap"]);
                sach.GiaBan = Convert.ToInt32(row["GiaBan"]);
                // ...

                bookList.Add(sach);
            }

            return bookList;
        }

        
    }
}
