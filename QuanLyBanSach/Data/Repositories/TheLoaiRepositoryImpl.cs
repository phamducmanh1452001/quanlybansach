﻿using System.Collections.Generic;
using System.Data;
using QuanLyBanSach.Data.Connections;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Data.Repositories
{
    internal class TheLoaiRepositoryImpl : TheLoaiRepository
    {

        public TheLoaiRepositoryImpl() { }

        bool TheLoaiRepository.NhapTheLoai(TheLoai theLoai)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Insert into TheLoai values(N'" + theLoai.MaTL + "',N'" + theLoai.TenTL + "')");
                return true;
            }
            catch {
                return false;
            }
        }

        List<TheLoai> TheLoaiRepository.LayTatCa()
        {
            var dt = DatabaseConnection.DataTable_Sql("Select MaTL,TenTL from TheLoai");
            return ConvertDataTableToList(dt);
        }

        private List<TheLoai> ConvertDataTableToList(DataTable dataTable)
        {
            List<TheLoai> typeList = new List<TheLoai>();

            foreach (DataRow row in dataTable.Rows)
            {
                TheLoai type = new TheLoai();
                type.MaTL = row["MaTL"].ToString();
                type.TenTL = row["TenTL"].ToString();
                // ...

                typeList.Add(type);
            }

            return typeList;
        }

        bool TheLoaiRepository.XoaTheLoaiTheoMa(string maTL)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Delete TheLoai where MaTL = '" + maTL + "'");
                return true;
            } catch {
                return false;
            }
            
        }

        bool TheLoaiRepository.CapNhatTheLoaiTheoMa(string maTL, TheLoai theLoai)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("update TheLoai set TenTL='" + theLoai.TenTL+ "' where MaTL='" + theLoai.MaTL + "'");
                return true;
            } catch
            {
                return false;
            }
        }
    }
}
