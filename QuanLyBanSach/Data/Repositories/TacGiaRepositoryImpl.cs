﻿using System;
using System.Collections.Generic;
using System.Data;
using QuanLyBanSach.Data.Connections;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Data.Repositories
{
    public class TacGiaRepositoryImpl : TacGiaRepository
    {
        public TacGiaRepositoryImpl() { }


        bool TacGiaRepository.NhapTacGia(TacGia tacGia)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("Insert into TacGia values(N'" + tacGia.MaTacGia + "', N'" + tacGia.TenTacGia + "', N'" + tacGia.LienLac + "')");
                return true;
            } catch {
                return false;
            }
        }

        bool TacGiaRepository.XoaTacGia(string maTG)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("delete from TacGia where MaTG='" + maTG + "'");
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        bool TacGiaRepository.CapNhatTacGia(string maTG, TacGia tacGia)
        {
            try
            {
                var _ = DatabaseConnection.DataTable_Sql("update TacGia set TenTG='" + tacGia.TenTacGia + "',LienLac='" + tacGia.LienLac + "' where MaTG='" + tacGia.MaTacGia + "'");
                return true;
            }
            catch
            {
                return false;
            }
        }

        List<TacGia> TacGiaRepository.LayTatCa()
        {
            var dt = DatabaseConnection.DataTable_Sql("Select MaTG,TenTG,LienLac from TacGia");
            return ConvertDataTableToList(dt);
        }

        private List<TacGia> ConvertDataTableToList(DataTable dataTable)
        {
            List<TacGia> tgList = new List<TacGia>();

            foreach (DataRow row in dataTable.Rows)
            {
                TacGia tg = new TacGia();
                tg.MaTacGia = row["MaTG"].ToString();
                tg.LienLac = row["LienLac"].ToString();
                tg.TenTacGia = row["TenTG"].ToString();
                // ...

                tgList.Add(tg);
            }

            return tgList;
        }
    }
}
