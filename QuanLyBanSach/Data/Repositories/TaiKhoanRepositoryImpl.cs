﻿
using System.Collections.Generic;
using QuanLyBanSach.Domain.Entities;
using QuanLyBanSach.Domain.Repositories;

namespace QuanLyBanSach.Data.Repositories
{
    internal class TaiKhoanRepositoryImpl : TaiKhoanRepository
    {
        public TaiKhoanRepositoryImpl() { }

        List<TaiKhoan> TaiKhoanRepository.LayDanhSachTaiKhoan()
        {
            var taiKhoanList = new List<TaiKhoan>
            {
                new TaiKhoan("TranHoangNam", "123456", false),
                new TaiKhoan("NguyenHoangNam", "123456", false),
                new TaiKhoan("Admin", "123", true)
            };
            return taiKhoanList;
        }
    }
}
