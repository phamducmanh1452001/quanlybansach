﻿
namespace QuanLyBanSach.Domain.Entities
{
    public class NhaXuatBan
    {
        public string MaNXB;
        public string TenNXB;
        public string DiaChi;
        public string DienThoai;

        public NhaXuatBan() { }

    }
}
