﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanSach.Domain.Entities
{
    public class TacGia
    {
        public string MaTacGia;
        public string TenTacGia;
        public string LienLac;

        public TacGia() { }
    }
}
