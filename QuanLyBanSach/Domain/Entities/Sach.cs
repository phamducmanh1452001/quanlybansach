﻿
namespace QuanLyBanSach.Domain
{
    public class Sach
    {
        public string MaSach;
        public string TenSach;
        public string MaTL;
        public string MaTG;
        public string MaNXB;
        public int SoLuongTon;
        public int GiaNhap;
        public int GiaBan;

        public Sach() { }
    }
}
