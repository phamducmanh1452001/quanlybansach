﻿using System.Collections.Generic;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Domain.Repositories
{
    public interface TaiKhoanRepository
    {
        List<TaiKhoan> LayDanhSachTaiKhoan(); 
    }
}
