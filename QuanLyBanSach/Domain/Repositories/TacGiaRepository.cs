﻿
using System.Collections.Generic;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Domain.Repositories
{
    public interface TacGiaRepository
    {
        bool NhapTacGia(TacGia tacGia);

        bool XoaTacGia(string maTG);

        bool CapNhatTacGia(string maTG, TacGia tacGia);

        List<TacGia> LayTatCa();
    }
}
