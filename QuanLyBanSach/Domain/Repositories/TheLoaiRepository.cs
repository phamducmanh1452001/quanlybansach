﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Domain.Repositories
{
    public interface TheLoaiRepository
    {
        bool NhapTheLoai(TheLoai theLoai);

        bool XoaTheLoaiTheoMa(string maTL);

        bool CapNhatTheLoaiTheoMa(string maTL, TheLoai theLoai);

        List<TheLoai> LayTatCa();
    }
}
