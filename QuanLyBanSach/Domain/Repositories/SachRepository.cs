﻿using System;
using System.Collections.Generic;

namespace QuanLyBanSach.Domain.Repositories
{
    public interface SachRepository
    {
        List<Sach> TimKiemSachTheoNhaXuatBan(String searchText);

        List<Sach> TimKiemSachTheoTheLoai(String searchText);

        List<Sach> TimKiemSachTheoTacGia(String searchText);

        List<Sach> LayTatCa();

        bool NhapSach(Sach sach);

        bool XoaSachTheoMa(string maSach);

        bool CapNhatSachTheoMa(string maSach, Sach sach);
    }
}
