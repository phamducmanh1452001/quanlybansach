﻿using System.Collections.Generic;
using QuanLyBanSach.Domain.Entities;

namespace QuanLyBanSach.Domain.Repositories
{
    public interface NhaXuatBanRepository
    {
        bool NhapNhaXuatBan(NhaXuatBan nxb);

        List<NhaXuatBan> LayTatCa();

        List<NhaXuatBan> TimKiemNhaXuatBanTheoMa(string maNXB);

        bool XoaNhaXuatBanTheoMa(string maNXB);

        bool CapNhatNhaXuatBanTheoMa(string maNXB, NhaXuatBan nxb);


    }
}
